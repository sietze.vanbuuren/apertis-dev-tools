#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import os
import tempfile

from test_util import SysrootServer
from test_util import templatedconfig
from test_util import should_succeed, should_fail
from test_util import add_auth_params, create_temp_netrc
from test_util import BASE_ARCHIVE, BASE_CONFIG, \
                      SYSROOTS, LATEST_SYSROOTS, \
                      CONFIG_FILES, BAD_URLS

# Utility functions
def check_archive(result, sysroot):
    with open(BASE_ARCHIVE.format(*sysroot), 'rb') as f:
        expected_content = f.read()
    with open(result['DownloadedArchive'], 'rb') as f:
        result_content = f.read()
    return result_content == expected_content

# Setup
server = SysrootServer()
server.start()

# Test "ade sysroot download --url" usage
for sysroot in SYSROOTS:
    with tempfile.TemporaryDirectory() as tmpdir:
        params = ['--url', server.base_url().format(*sysroot), '--distro', sysroot[0],
                  '--release', sysroot[1], '--arch', sysroot[2]]
        add_auth_params(params, sysroot)
        should_succeed('sysroot', '--path', tmpdir, 'download', *params,
                       check=lambda x: check_archive(x, sysroot))

# Test "ade sysroot download --url" usage with user and password in url
for sysroot in SYSROOTS:
    with tempfile.TemporaryDirectory() as tmpdir:
        params = ['--url', server.base_url_with_auth().format(*sysroot), '--distro', sysroot[0],
                  '--release', sysroot[1], '--arch', sysroot[2]]
        should_succeed('sysroot', '--path', tmpdir, 'download', *params,
                       check=lambda x: check_archive(x, sysroot))

# Test "ade sysroot download --url" usage with user and password in netrc file
temp_netrc = create_temp_netrc(server)
for sysroot in SYSROOTS:
    with tempfile.TemporaryDirectory() as tmpdir:
        params = ['--url', server.base_url().format(*sysroot), '--distro', sysroot[0],
                  '--release', sysroot[1], '--arch', sysroot[2], '--netrc', temp_netrc.name]
        should_succeed('sysroot', '--path', tmpdir, 'download', *params,
                       check=lambda x: check_archive(x, sysroot))
temp_netrc.close()

# Test "ade sysroot download --distro --release --arch" usage
for sysroot in SYSROOTS:
    with tempfile.TemporaryDirectory() as tmpdir:
        with templatedconfig(server, BASE_CONFIG.format(*sysroot)) as config:
            params = ['--distro', sysroot[0], '--release', sysroot[1], '--arch', sysroot[2]]
            add_auth_params(params, sysroot)
            should_succeed('sysroot', '--config', config, '--path', tmpdir, 'download', *params,
                            check=lambda x: check_archive(x, sysroot))

# Test "ade sysroot download --distro --release --arch" usage with different config files
for config_file in CONFIG_FILES:
    with templatedconfig(server, config_file) as config:
        for sysroot in LATEST_SYSROOTS:
            with tempfile.TemporaryDirectory() as tmpdir:
                params = ['--distro', sysroot[0], '--release', sysroot[1], '--arch', sysroot[2]]
                add_auth_params(params, sysroot)
                should_succeed('sysroot', '--config', config, '--path', tmpdir, 'download', *params,
                               check=lambda x: check_archive(x, sysroot))

# Test error cases
with tempfile.TemporaryDirectory() as tmpdir:
    for url in BAD_URLS:
        url.replace("HOST", server.host())
        params = ['--url', url, '--distro', 'eraroj', '--release', '16.09', '--arch', 'armhf']
        should_fail('sysroot', '--path', tmpdir, 'download', *params)

# Tear down
server.stop()
