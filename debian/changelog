apertis-dev-tools (0.2023.11) apertis; urgency=medium

  * apertis-pkg-pull-updates: Make fetching of backports and proposed-updates
    repositories optional.
    The Apertis pipeline imports backport and/or proposed-updates versions
    by default, this is not needed and leads to miss some security updates.
    Because, importing versions from backports and proposed-updates is not
    part of our standard workflow, this should only be optional to avoid
    confusing the pipeline.
  * apertis-pkg-pull-updates: pull updates from *-updates repositories
    Updates from *-proposed-updates repositories are not pulled anymore
    by default because it can contain undesirable updates for Apertis.
    "proposed-updates" contains updates not officialy part of Debian
    contrary to "updates".
    However, we still want updates from *-updates repositories instead.

 -- Dylan Aïssi <daissi@debian.org>  Mon, 15 Jan 2024 10:14:54 +0100

apertis-dev-tools (0.2023.10) apertis; urgency=medium

  * import-debian-package: avoid oversized commit messages
    `gbp import-dsc` puts all the new changelog entries in the commit
    message, generating big walls of text when, for instance, importing a new
    package for the first time.
    GitLab then puts the whole log message in the `CI_COMMIT_MESSAGE` env
    var, which makes the runner to fail during the executor preparation
    section with the error:
        exec /usr/bin/dumb-init: argument list too long
    To avoid that, trim the message to only keep the first line.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Thu, 28 Sep 2023 16:02:24 +0200

apertis-dev-tools (0.2023.9) apertis; urgency=medium

  * ade: Turn all from_* methods into proper classmethods
  * ade: Sort imports with isort
  * ade: Format with black
  * ade: Drop unused imports as reported by flake8
  * ade: Fix usage of undefined variable
  * ade: Use raw strings for regexes avoiding escaping conflicts
  * ade: Drop unused exception capturing variables
  * ade: Avoid bare except: to make flake8 happy
  * ade: Use a more descriptive variable name in do_sysroot_list()
  * Add black-compatible presets for flake8
  * ci: Enfore linting on tools/ade

 -- Emanuele Aina <emanuele.aina@collabora.com>  Fri, 15 Sep 2023 08:20:50 +0000

apertis-dev-tools (0.2023.8) apertis; urgency=medium

  * ade: Move common check to new functions
  * ade: Fallback to /etc/os-release
  * ade: Fallback mechanism for arch detection

 -- Walter Lozano <walter.lozano@collabora.com>  Tue, 22 Aug 2023 13:38:14 -0300

apertis-dev-tools (0.2023.7) apertis; urgency=medium

  * Mountimage tool extended: Now able to mount images without a clear root partition
  * Some error corrections at mountimage tool

 -- Thomas Mittelstaedt <thomas.mittelstaedt@de.bosch.com>  Fri, 25 Aug 2023 13:16:12 +0000

apertis-dev-tools (0.2023.6) apertis; urgency=medium

  * Refresh the automatically detected licensing information

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Fri, 04 Aug 2023 14:49:19 +0200

apertis-dev-tools (0.2023.5) apertis; urgency=medium

  * apertis-abi-compare: don't fail in case of soname change.
    When the soname changes, the name of the binary package changes
    accordingly. In this case, ABI breakage is expected thus there is
    no need to run abi-compliance-checker. apertis-abi-compare detects
    this case and report the soname change without failing.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Tue, 20 Jun 2023 17:44:09 +0200

apertis-dev-tools (0.2023.4) apertis; urgency=medium

  * import-debian-package: add a mandatory flag --reason.
    This flag requires a string explaining the reason of importing
    a package. This reason will be written in debian/changelog and
    will be useful for a future reconsideration of the value of this
    package in Apertis.
  * Add the new mandatory flag --reason in scripts from tests/

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Mon, 19 Jun 2023 17:00:16 +0200

apertis-dev-tools (0.2023.3) apertis; urgency=medium

  * Make apertis-dev-tools depend on sudo:
    apertis-abi-compare relies on sudo and fails if sudo
    is not available.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Tue, 13 Jun 2023 16:14:16 +0200

apertis-dev-tools (0.2023.2) apertis; urgency=medium

  * Update debian/apertis/copyright

 -- Vignesh Raman <vignesh.raman@collabora.com>  Tue, 13 Jun 2023 14:22:51 +0530

apertis-dev-tools (0.2023.1) apertis; urgency=medium

  * Bump Standards-Version to 4.6.2
  * Update debian/copyright

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Thu, 27 Apr 2023 09:41:48 +0200
 
apertis-dev-tools (0.2023.0) apertis; urgency=medium

  * import-debian-package: Create repository with CI disabled

 -- Walter Lozano <walter.lozano@collabora.com>  Wed, 26 Apr 2023 16:02:13 -0300

apertis-dev-tools (0.2022.10) apertis; urgency=medium

  * Mountimage: Extend --fstab option for partition type = SD_GPT_ESP

 -- Thomas Mittelstaedt <thomas.mittelstaedt@de.bosch.com>  Thu, 13 Apr 2023 14:38:36 +0000

apertis-dev-tools (0.2022.9) apertis; urgency=medium

  * mountimage to mount system images added

 -- Thomas Mittelstaedt <thomas.mittelstaedt@de.bosch.com>  Thu, 02 Feb 2023 09:35:34 +0000

apertis-dev-tools (0.2022.8) apertis; urgency=medium

  * Add debian/apertis/lintian to enable lintian for
    apertis-dev-tools package.
  * Fix lintian errors and warnings.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Wed, 21 Dec 2022 09:21:47 +0530

apertis-dev-tools (0.2022.7) apertis; urgency=medium

  * import-debian-package: when "upstream" is set to "unstable", use "sid"
      instead for consistency across all Apertis packages.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Mon, 19 Dec 2022 11:43:20 +0100

apertis-dev-tools (0.2022.6) apertis; urgency=medium

  * apertis-abi-compare:
     - add a final summary
     - fix sporadic issue with std headers

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Tue, 18 Oct 2022 15:21:42 +0000

apertis-dev-tools (0.2022.5) apertis; urgency=medium

  * apertis-abi-compare:
     - Add support of ARM packages via cross-compiling

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Tue, 18 Oct 2022 10:58:44 +0200

apertis-dev-tools (0.2022.4) apertis; urgency=medium

  * apertis-abi-compare:
     - add a cmake module generating descriptor file at build time

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Thu, 29 Sep 2022 09:39:06 +0000

apertis-dev-tools (0.2022.3) apertis; urgency=medium

  * apertis-abi-compare:
     - print a message in case of the ABI check fails due to errors
     - correct 'apt update' command

 -- Tino Lippold <Tino.Lippold@de.bosch.com>  Mon, 19 Sep 2022 07:19:12 +0000

apertis-dev-tools (0.2022.2) apertis; urgency=medium

  * apertis-abi-compare:
     - run 'apt update' before 'apt install' to be sure having the latest versions
     - run 'apt install' with '-y' argument because noninteractive call
     - force flushing for stdout to avoid mixed ordering of stdout and stderr outputs

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Thu, 25 Aug 2022 12:01:00 +0200

apertis-dev-tools (0.2022.1) apertis; urgency=medium

  * apertis-abi-compare:
      - support packages providing multiple libraries
      - make it compatible with python 3.7 in Apertis v2021
      - fix several issues

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Tue, 26 Jul 2022 14:49:34 +0200

apertis-dev-tools (0.2022.0) apertis; urgency=medium

  * tools: Add a script to easily run Apertis images in QEMU

 -- Ryan Gonzalez <ryan.gonzalez@collabora.com>  Thu, 21 Jul 2022 16:18:00 -0500

apertis-dev-tools (0.2021.14) apertis; urgency=medium

  * Split the apertis-rebase-upstream-kernel script

 -- Detlev Casanova <detlev.casanova@collabora.com>  Thu, 07 Jul 2022 13:15:51 -0400

apertis-dev-tools (0.2021.13) apertis; urgency=medium

  * Add apertis-push-upstream-kernel apertis-rebase-upstream-kernel
    scripts

 -- Detlev Casanova <detlev.casanova@collabora.com>  Thu, 30 Jun 2022 17:08:41 -0400

apertis-dev-tools (0.2021.12) apertis; urgency=medium

  * apertis-abi-compare: return the value obtained from abi-compliance-checker

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Tue, 14 Jun 2022 14:45:16 +0200

apertis-dev-tools (0.2021.11) apertis; urgency=medium

  * Add a new tool: apertis-abi-compare
  * Add abi-compliance-checker in Depends

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Fri, 27 May 2022 14:37:47 +0200

apertis-dev-tools (0.2021.10) apertis; urgency=medium

  * tools: apertis-pkg-pull-updates: Pull updates from debian backports

 -- Ariel D'Alessandro <ariel.dalessandro@collabora.com>  Wed, 18 May 2022 11:34:22 -0300

apertis-dev-tools (0.2021.9) apertis; urgency=medium

  * apertis-pkg-merge-updates: fast-forward the main debian branch
      after a Debian release point

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Fri, 13 May 2022 14:33:34 +0200

apertis-dev-tools (0.2021.8) apertis; urgency=medium

  * Add a script to switch between coreutils and rust-coreutils

 -- Detlev Casanova <detlev.casanova@collabora.com>  Mon, 21 Feb 2022 14:09:42 -0500

apertis-dev-tools (0.2021.7) apertis; urgency=medium

  * pkg-merge-updatesi: Try to fast-forward before rebasing

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 29 Jan 2022 14:05:37 +0000

apertis-dev-tools (0.2021.6) apertis; urgency=medium

  * import-debian-package: Allow creating/pushing to remote repository

 -- Ariel D'Alessandro <ariel.dalessandro@collabora.com>  Fri, 07 Jan 2022 09:08:09 -0300

apertis-dev-tools (0.2021.5) apertis; urgency=medium

  * Drop application bundle support

 -- Walter Lozano <walter.lozano@collabora.com>  Sun, 14 Nov 2021 16:39:13 -0300

apertis-dev-tools (0.2021.4) apertis; urgency=medium

  * Add import-debian-package
  * Add apertis-pkg-{merge,pull}-updates
  * d/a/local-gitlab-ci.yml: Update apt

 -- Frédéric Danis <frederic.danis@collabora.com>  Mon, 12 Oct 2021 18:03:07 +0200

apertis-dev-tools (0.2021.3) apertis; urgency=medium

  * Run the local pipeline whenever the parent pipeline triggers it

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sun, 03 Oct 2021 19:29:07 +0000

apertis-dev-tools (0.2021.2) apertis; urgency=medium

  * Fix installability on v2022dev2. Demote dependency on ribchester to a
    suggests since it is not available in v2022dev2.

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 31 May 2021 10:22:03 +0000

apertis-dev-tools (0.2021.1) apertis; urgency=medium

  * debian/apertis/gitlab-ci.yml: Drop since we use an external definition
  * d/a/local-gitlab-ci.yml: Do not run on tags

 -- Emanuele Aina <emanuele.aina@collabora.com>  Tue, 18 May 2021 00:07:24 +0000

apertis-dev-tools (0.2021.0) apertis; urgency=medium

  * ade sysroot: Allow installation of amd64 sysroots
  * Run automated tests on the CI

 -- Emanuele Aina <emanuele.aina@collabora.com>  Wed, 03 Mar 2021 12:26:30 +0000

apertis-dev-tools (0.2020.1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Thu, 11 Feb 2021 09:07:58 +0000

apertis-dev-tools (0.2020.1) apertis; urgency=medium

  * gitlab-ci: Link to the Apertis GitLab CI pipeline definition.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Wed, 15 Jan 2020 11:18:26 +0800

apertis-dev-tools (0.2019.1) apertis; urgency=medium

  * Add verbose option to supress error message

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Thu, 10 Oct 2019 16:19:26 +0530

apertis-dev-tools (0.2019.0) apertis; urgency=medium

  [ Frédéric Danis ]
  * Support username and password set in URL
  * Add support of .netrc file
  * Add username:password tests for sysroot latest and update commands

  [ Ritesh Raj Sarraf ]
  * Handle percent encoded characters

  [ Frédéric Danis ]
  * Fix crash for URLs without password

 -- Emanuele Aina <emanuele.aina@collabora.com>  Fri, 23 Aug 2019 13:33:42 +0000

apertis-dev-tools (0.2019~dev0.1) apertis; urgency=medium

  * Handle the v2019* versioning schema by being less strict

 -- Emanuele Aina <emanuele.aina@collabora.com>  Thu, 04 Apr 2019 20:06:09 +0000

apertis-dev-tools (0.1903.1) 19.03; urgency=medium

  * Mount the user's home directory is r/w at the same path as outside the
    devroot (Apertis: T5626).

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Tue, 08 Jan 2019 11:07:05 +0100

apertis-dev-tools (0.1812.4) 18.12; urgency=medium

  * Install everything from /usr/bin.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Wed, 28 Nov 2018 12:00:56 +0100

apertis-dev-tools (0.1812.3) 18.12; urgency=medium

  * Depend on systemd-container.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Wed, 28 Nov 2018 11:41:53 +0100

apertis-dev-tools (0.1812.2) 18.12; urgency=medium

  * Add devroot-enter tool.
  * Change the formatting style for the ade manpages.
  * Fix the regex in gbp.conf.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Wed, 28 Nov 2018 10:20:14 +0100

apertis-dev-tools (0.1812.1) 18.12; urgency=medium

  [ Ritesh Raj Sarraf ]
  * Add tests/test-export test script (Apertis: T4427).

  [ Andrej Shadura ]
  * Force allocating a PTY when running gdbserver (Apertis: T4463).
  * Build-Depend on flatpak and ostree since test-export requires them.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Thu, 18 Oct 2018 12:08:02 +0200

apertis-dev-tools (0.1712.1) 17.12; urgency=medium

  [ Frédéric Dalleau ]
  * ade: fix typo
  * ade: Detect gdbserver process termination (Apertis: T4462)

 -- Frédéric Dalleau <frederic.dalleau@collabora.com>  Wed, 10 Jan 2018 10:53:34 +0000

apertis-dev-tools (0.1712.0) 17.12; urgency=medium

  * append `./` when checking image_version file

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Mon, 18 Dec 2017 18:01:59 +0100

apertis-dev-tools (0.1706.2) 17.06; urgency=medium

  [ Justin Kim ]
  * ade: Add --native option

  [ Simon McVittie ]
  * ade info: Don't crash if an entry point has no icon

 -- Simon McVittie <smcv@collabora.com>  Wed, 24 May 2017 17:17:46 +0100

apertis-dev-tools (0.1706.1) 17.06; urgency=medium

  * ade: configure: Don't modify caller-supplied cflags, ldflags in-place
  * ade: configure: Don't ignore caller-supplied arguments (Apertis: T3978)
  * ade: Reject unknown arguments if they will not be used
  * ade: Provide conventional handling for the -- pseudo-option
  * ade: Warn on unknown arguments without -- pseudo-argument
  * ade: Correct order of deprecated passthrough arguments

 -- Simon McVittie <smcv@collabora.com>  Tue, 16 May 2017 17:04:47 +0100

apertis-dev-tools (0.1706.0) 17.06; urgency=medium

  [ Frédéric Dalleau ]
  * ade: Append distro, release, and arch to sysroot file name (Apertis: T3849)

 -- Frédéric Dalleau <frederic.dalleau@collabora.com>  Mon, 08 May 2017 08:11:06 +0000

apertis-dev-tools (0.1703.4) 17.03; urgency=medium

  [ Frédéric Dalleau ]
  * Fix parsing of sysroot id (Apertis: T3777)
  * Fix usage of undeclared InvalidSysrootError (Apertis: T3777)
  * Rework run_cmd in test_util.py to simplify API (Apertis: T3777)
  * Add sysroot tag parser test (Apertis: T3777)
  * Dummy project to exercise ade configure and build (Apertis: T3777)
  * Add sysroot path option to ade configure
  * test-configure to exercise ade configure (Apertis: T3777)

 -- Frédéric Dalleau <frederic.dalleau@¢ollabora.com>  Thu, 30 Mar 2017 17:37:41 +0000

apertis-dev-tools (0.1703.3) 17.03; urgency=medium

  [ Sjoerd Simons ]
  * version: Remove build identifier parsing
  * version: Don't force releases to be named in the form of YY.QQ
    (Apertis: T3555)
  * tests_: Add test for a non-numeric relesae name

 -- Frédéric Dalleau <frederic.dalleau@¢ollabora.com>  Fri, 17 Mar 2017 08:43:51 +0000

apertis-dev-tools (0.1703.2) 17.03; urgency=medium

  [ Thushara Malali Somesha ]
  * debian/control: Add ribchester package dependency

  [ Frédéric Dalleau ]
  * ade: Change build directory for configure and build (Apertis: T3557)
  * make distclean before configuring an existing folder (Apertis: T3557)
  * Use sudo to install bundles on the simulator (Apertis: T3556)

 -- Frédéric Dalleau <frederic.dalleau@¢ollabora.com>  Fri, 10 Mar 2017 10:54:25 +0000

apertis-dev-tools (0.1703.1) 17.03; urgency=medium

  * Add man pages to deb package
  * ade: Use decode() function to bytes object

 -- Justin Kim <justin.kim@collabora.com>  Wed, 04 Jan 2017 19:24:26 +0900

apertis-dev-tools (0.1612.6) 16.12; urgency=medium

  [ Sjoerd Simons ]
  * Add dependency on flatpak
  * Allow all auth methods

  [ Louis-Francis Ratté-Boulianne ]
  * Use default entry point to start application if not specified
  * Wait for the GDB server to be listening before doing anything else
  * Add --no-interactive option to the debug command

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Fri, 16 Dec 2016 15:45:07 +0100

apertis-dev-tools (0.1612.5) 16.12; urgency=medium

  [ Louis-Francis Ratté-Boulianne ]
  * Only give compiler hint if not compiling natively
  * Unpack arguments list when calling run command
  * Fix run command documentation
  * Add utility function to unwrap target if device
  * Add support start a remote application under a debugger

  [ Sjoerd Simons ]
  * Set debug-file-directory when using a sysroot
  * Run gdb as a subprocess, not execv'd
  * New release
    * Supports for running remote applications under a debug (Apertis: T2993)

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Thu, 15 Dec 2016 12:03:04 +0100

apertis-dev-tools (0.1612.4) 16.12; urgency=medium

  * Add dependency on python3-gi

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Thu, 15 Dec 2016 11:12:54 +0100

apertis-dev-tools (0.1612.3) 16.12; urgency=medium

  [ Justin Kim ]
  * Add python3-paramiko dependency

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Thu, 15 Dec 2016 10:55:57 +0100

apertis-dev-tools (0.1612.2) 16.12; urgency=medium

  [ Guillaume Desmottes ]
  * SysrootServer: set SO_REUSEADDR flag on the socket

  [ Sjoerd Simons ]
  * test: Don't hardcode the port to listen on

  [ Louis-Francis Ratté-Boulianne ]
  * Add LatestURL key to sysroot latest command
  * Add 'installed' command to show currently installed sysroot version
  * Add support for authentication when retrieving remote data
  * Add class for installed sysroot
  * Create SysrootManager class to handle sysroot operations
  * Add info command to retrieve information about an ADE object
  * Add device class and option to retrieve the image version
  * Add support for x86_64 architecture
  * Add SDK and Simulator classes to handle these targets
  * Create cli tool to cross-compile against a specific chroot

  [ Sjoerd Simons ]
  * Don't try to parse non-existing configuration file
  * Raise a proper exception, not a string
  * Create /opt/sysroot via tmpfiles
  * Add dummy install target
  * Be a lot more specific about what to install

  [ Guillaume Desmottes ]
  * ade: display sysroot versions in case of mismatch

  [ Sjoerd Simons ]
  * Don't force a keyfile
  * Clarify unsupported architecture error
  * Sort out the various machine type names
  * Don't join absolute paths
  * Actually use a target specific compiler
  * Recommend an ARM cross-compiler

  [ Louis-Francis Ratté-Boulianne ]
  * Add support for project in info command
  * Add export command to create application bundles
  * Add support for bundles in info command
  * Add install and uninstall commands
  * Add run command to start application

  [ Sjoerd Simons ]
  * Raise an exception if an ssh command fails
  * Use ribchesterctl not ribchester-bundle

  [ Louis-Francis Ratté-Boulianne ]
  * Small fixes to bundle creation

  [ Sjoerd Simons ]
  * New release
  * Supports creating a bundle and transferring it to target (Apertis: T2989)
  * Supports cross-compiling applications bundles (Apertis: T2988)

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Thu, 15 Dec 2016 10:42:36 +0100

apertis-dev-tools (0.1612.1) 16.12; urgency=medium

  [ Louis-Francis Ratté-Boulianne ]
  * Add basic project structure (Apertis: T2984)
  * Add tool to install/update sysroot
  * Add documentation for sysroot management command
  * Add --config option for sysroot command
  * Add output format option (--format) and machine parseable format
  * Add unit tests for sysroot management tool
  * ade: Expand ~ in configured paths
  * Use pkg-config.multiarch file to determine architecture (Apertis: T2984)

  [ Guillaume Desmottes ]
  * debian: add dep on python modules

 -- Guillaume Desmottes <guillaume.desmottes@collabora.co.uk>  Fri, 09 Dec 2016 10:39:54 +0100
