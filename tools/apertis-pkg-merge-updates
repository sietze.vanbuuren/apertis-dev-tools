#!/usr/bin/env python3
# SPDX-License-Identifier: MPL-2.0
#
# Copyright © 2019 Collabora Ltd
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import argparse
import json
import os
import shlex
import subprocess
import sys
import tempfile
import urllib.request

from sh.contrib import git

from pathlib import Path

from debian.debian_support import Version
from debian.changelog import Changelog, VersionError, format_date

APERTIS_CI_NAME = 'Apertis CI'
APERTIS_CI_EMAIL = 'devel@lists.apertis.org'

def git_dir() -> str:
    return git('rev-parse', '--git-dir').strip('\n')

def run(cmd, **kwargs):
  quoted = ' '.join(shlex.quote(i) for i in cmd)
  print('running', quoted)
  return subprocess.run(cmd, **kwargs)

def configure_git_merge():
    with Path(git_dir()) / 'info' / 'attributes' as attributes_path:
      if not attributes_path.is_file():
        try:
            attributes_path.parent.mkdir(exist_ok=True)
            attributes_path.write_text('debian/changelog merge=dpkg-mergechangelogs')
        except IOError as e:
            print(f'Failed to set up attributes: {e}')
    git('config', 'merge.dpkg-mergechangelogs.name', 'debian/changelog merge driver')
    git('config', 'merge.dpkg-mergechangelogs.driver', 'dpkg-mergechangelogs %O %A %B %A')

def configure_git_user(name, email):
  git('config', 'user.email', email)
  git('config', 'user.name', name)

def prepare_git_repo(upstream_branch):
  run(['git', 'branch', '-f', upstream_branch, 'origin/' + upstream_branch])
  configure_git_user(APERTIS_CI_NAME, APERTIS_CI_EMAIL)
  configure_git_merge()

def get_package_name():
  with open("debian/changelog") as f:
      ch = Changelog(f, max_blocks=1)
  return ch.package

def get_git_branch_version(branch: str):
  ch = Changelog(git.show(f'{branch}:debian/changelog'), max_blocks=1)
  return ch.version

def get_local_version(suite):
  upstream_branch = debian_branch(suite)
  return get_git_branch_version(upstream_branch)

def get_current_branch_name():
    branch = git('rev-parse', '-q', '--verify', '--symbolic-full-name', 'HEAD', _ok_code=[0, 1]).strip('\n')
    return branch.replace('refs/heads/', '', 1)

def bump_version(version: Version, osname: str, changes: list, release: bool = False):
    with Path("debian/changelog") as f:
        ch = Changelog(f.read_text())
        if version <= ch.version:
            raise VersionError("The new version must be greater than the last one.")
        ch.new_block(package=ch.package,
                     version=version,
                     distributions=osname if release else 'UNRELEASED',
                     urgency='medium',
                     author=('%s <%s>' % (APERTIS_CI_NAME, APERTIS_CI_EMAIL)),
                     date=format_date())
        ch.add_change('')
        for change in changes:
            ch.add_change(f'  * {change}')
        ch.add_change('')
        f.write_text(str(ch))
        git.add('-f', f)

def main():
  parser = argparse.ArgumentParser(description='Merge updates from the upstream repositories to the derivative branch')
  parser.add_argument('--package', dest='package', type=str, help='the package name (e.g. glib2.0)') # TODO: figure this out from the repo
  parser.add_argument('--osname', dest='osname', type=str, default='apertis', help='the OS name for the distribution field in the changelog')
  parser.add_argument('--downstream', dest='downstream', type=str, help='the downstream branch (e.g. apertis/v2020dev0)')
  parser.add_argument('--upstream', dest='upstream', type=str, required=True, help='the upstream branch (e.g. debian/buster)')
  parser.add_argument('--local-version-suffix', dest="local_suffix", type=str, default="+apertis", help='the local version suffix to be used in the new changelog entry')
  parser.add_argument('--rebase-range', dest="rebase_range", type=str, help='rebase changes from this range as well (e.g. apertis/v2021..downstream/v2021 when targeting downstream/v2022dev0)')
  args = parser.parse_args()
  package_name = args.package

  package_name = args.package or get_package_name()
  print('source package', package_name)
  prepare_git_repo(args.upstream)

  downstream_version = get_git_branch_version(args.downstream)
  upstream_version = get_git_branch_version(args.upstream)
  print(f'downstream {args.downstream} {downstream_version}')
  print(f'upstream {args.upstream} {upstream_version}')

  rebase_range = args.rebase_range
  rebased_commits = []
  fast_forwarded = False

  if downstream_version >= upstream_version and not rebase_range:
      print("Downstream is already up to date nothing to do")
      return
  try:
    if not rebase_range:
      print(f'Merging from {args.upstream}')
      git('merge', args.upstream, '--no-ff', '-m', f'Merge updates from {args.upstream}', '--stat', _fg=True)
    else:
      rebase_base, rebase_tip = [f'origin/{ref}' for ref in rebase_range.split('..')]
      print(f"Attempt to fast-forward to {rebase_tip}")
      o = git('merge', '--ff-only', rebase_tip, _ok_code=[0, 128], _out='/dev/stdout', _err='/dev/stderr')
      if o.exit_code == 0:
        print(f"⏩ Successfully fast-forwarded {args.downstream} to {rebase_tip}")
        sys.exit(0)
      else:
        print(f'Rebase changes from {rebase_base} to {rebase_tip} onto {args.upstream}:')
        source_commits = git('log', '--oneline', f'{rebase_base}..{rebase_tip}').splitlines()
        for c in source_commits:
          print(' ', c)
        current_branch = get_current_branch_name()
        git('rebase', '--onto', args.upstream, rebase_base, rebase_tip)
        rebased_commits = git('log', '--oneline', f'{args.upstream}..').splitlines()
        print(f'⤴️ Rebased {len(rebased_commits)} commits:')
        for c in rebased_commits:
          print(' ', c)
        git('branch', '-f', current_branch)
        git('checkout', current_branch)
  except Exception as e:
    print('🛑 Merge failed:')
    print(e)
    sys.exit(1)

  o = git('diff', '--exit-code', args.upstream, ':!debian/changelog', ':!debian/apertis/*', _ok_code=[0,1])
  if o.exit_code == 1:
    # we carry some changes in addition to changelog entries
    # and metadata under debian/apertis, so someone should
    # re-summarize the remaining changes
    version = upstream_version.full_version + args.local_suffix + '1'
    bump_version(
        version,
        args.osname,
        ['PLEASE SUMMARIZE remaining Apertis changes'],
        release=False
    )
  else:
    # no changes, but we add a suffix anyway
    msg = [f'Sync from {args.upstream}.']
    if rebased_commits:
        msg.append(f"Rebase changes from {rebase_range}.")
    version = upstream_version.full_version + args.local_suffix + '0'
    bump_version(
        version,
        args.osname,
        msg,
        release=True
    )
  git('commit', 'debian/changelog', '-m', f'Release {package_name} version {version}')
  action = "rebased" if rebased_commits else "merged"
  print(f"☯️  Successfully {action} {args.upstream} in {args.downstream} with version {version}")

if __name__ == '__main__':
  main()
